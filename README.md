# WaytoB - Developer role task

The aim of this short task is to test your analytical and programming skills by implementing the following problem.

---

## The task

The task is to compute a straight line of interpolated points **20 metres** apart between two locations on a map. The points should **evenly** spaced and plotted on the map.

The two locations should be determined by listening for user inputted clicks on the map and the logic should be as follows:

1. A user clicks anywhere on the map, a marker should be dropped with a the label 'depart'
2. A user clicks a second time on the map, a marker should be dropped with a the label 'destination'
3. Immediatley after the destination pin is marked, the interpolated points should be calculated and plotted
4. The distance between the depart and destination should be calculated and displayed in the relevant input field
5. The number of interpolated points should also displayed in the relevant input field
6. Any further clicks should result in an alert to say the calculation has been completed and to reset the map
7. Hitting the 'Clear Map' button should reset everything and allow the user to plot another 2 markers

See example below of what the final solution should look like:

![Solution Gif](https://media.giphy.com/media/8PpsNMNs3ZnNBIXxd8/giphy.gif)

---

## Getting Started

This repository can be used as a template and includes a Google Maps API key, so you will not need to configure anything.

1. Create a new empty repository on your personal github/bitbucket/other account called waytoB_Test

2. Create a bare clone of **this** reposistory to your local machine using the following command
3. ``` git clone --bare https://bitbucket.org/robbiefryers/waytoB_Test.git```
4. Mirror push the template repo you just cloned into your newly created personal repo with the following commands
5. ``` cd waytoB_Test.git ```
6. ``` git push --mirror https://github.com/your_account/waytoB_Test.git ```
7. Remove the bare clone repository from your machine you created in step 2 
8. ``` cd .. ```
9. ``` rm -rf waytoB_Test.git ``` 

Your local machine should now be empty and you should have an independent copy of the template in your online github/bitbucket repo

Modify the template and implement the above logic using javascript. You may want to clone the template back to your local machine, this time from your personal repository.


---


## Submitting your solution

Send a URL to your personal repository to robbie@waytoB.com so I can clone and check it.



